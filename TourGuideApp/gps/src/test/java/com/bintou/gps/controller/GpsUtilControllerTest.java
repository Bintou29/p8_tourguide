package com.bintou.gps.controller;

import gpsUtil.location.Attraction;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class GpsUtilControllerTest {

    @Autowired
    private GpsUtilController gpsUtilController;


    @Test
    public void getAllAttractionsTest() {
        List<Attraction> attractionList = gpsUtilController.getAttractions();
        Assertions.assertNotNull(attractionList);
        Assertions.assertEquals(attractionList.size(), 26);
    }

}
