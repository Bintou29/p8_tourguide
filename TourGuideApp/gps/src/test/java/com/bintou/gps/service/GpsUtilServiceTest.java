package com.bintou.gps.service;

import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@SpringBootTest
@AutoConfigureMockMvc
public class GpsUtilServiceTest {

    @MockBean
    private GpsUtilService gpsUtilService;


    @Test
    public void getAllAttractionsTest() throws Exception {
        List<Attraction> attractionList = new ArrayList<>();
        attractionList.add(new Attraction("Disneyland", "Anaheim", "CA", 33.817595, -117.922008));
        attractionList.add(new Attraction("Jackson Hole", "Jackson Hole", "WY", 43.582767, -110.821999));
        attractionList.add(new Attraction("Mojave National Preserve", "Kelso", "CA", 35.141689, -115.510399));
        attractionList.add(new Attraction("Joshua Tree National Park", "Joshua Tree National Park", "CA", 33.881866, -115.90065));
        attractionList.add(new Attraction("Buffalo National River", "St Joe", "AR", 35.985512, -92.757652));

        Mockito.when(gpsUtilService.getAttractions()).thenReturn(attractionList);
        Assertions.assertEquals(attractionList.size(), 5);

    }

    @Test
    public void getUserLocationTest() {
        UUID uuid = UUID.randomUUID();
        double longitude = -3.851052;
        double latitude = 54.986509;

        VisitedLocation visitedLocation = new VisitedLocation(uuid, new Location(latitude, longitude), new Date());
        Mockito.when(gpsUtilService.getUserLocation(uuid)).thenReturn(visitedLocation);

        System.out.println(uuid); // 16e11694-e2df-432a-a22c-00a20e24828d
        Assertions.assertNotNull(visitedLocation);
    }


}
