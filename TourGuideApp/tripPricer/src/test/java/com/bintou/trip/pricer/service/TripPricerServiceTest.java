package com.bintou.trip.pricer.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import tripPricer.Provider;

import java.util.List;
import java.util.UUID;

@SpringBootTest
public class TripPricerServiceTest {

    @MockBean
    private TripPricerService tripPricerService;

    @Test
    public void getPriceTest() {

        UUID uuid1 = UUID.randomUUID();
        System.out.println("uuid = " + uuid1);
        UUID uuid2 = UUID.randomUUID();
        System.out.println("uuid = " + uuid2);

        List<Provider> providerList = tripPricerService.getPrice("test", uuid1, 1, 0, 3, 1);
        Mockito.when(tripPricerService.getPrice("test", uuid1, 1, 0, 3, 1)).thenReturn(providerList);
        Assertions.assertEquals(providerList.size(), 0);
    }
}
